Welcome to the Mohawk+T Testcase Repository
===========================================

This repository contains all of the test cases used by Mohawk+T in 
both the conference and journal paper (see source below).

If you wish to run any of these testcases, just go into one of the other 
Mohawk+T tools and clone this repo into the "data/" folder.

This repository splits the testcases into the formats that Mohawk+T uses 
as well as the comparison tools created by Rainse et. al. and Uzun et. al.

 * mohawkT - Holds testcases that were created by Shahen et. al. or converted 
             from Ranise et. al's Testsuite B and Testsuite C
 * asasptimensa - Contains the raw Testsuite B testcases, provided by Rainse et. al.
             and the converted form of the other testcases into Asasptime SA format.
 * asasptimesa - Contains the raw Testsuite C testcases, provided by Rainse et. al.
             and the converted form of the other testcases into Asasptime SA format.
 * trole - Contains converted forms of Mohawk+T files